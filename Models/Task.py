from app import db, ma
from Models.Tenant import Tenant
from Models.Ticket import Ticket

class Task(db.Model):
    __tablename__ = "tasks"
    task_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    priority = db.Column(db.String(50))
    user_id = db.Column(db.Integer, db.ForeignKey("users.user_id"))
    user = db.relationship("User", backref="tasks")
    tenant_id = db.Column(db.Integer, db.ForeignKey("tenants.tenant_id"))
    tenant = db.relationship("Tenant", backref="tasks")
    ticket_id = db.Column(db.Integer, db.ForeignKey("tickets.ticket_id"))
    ticket = db.relationship("Ticket", backref="tasks")

    def __repr__(self):
        return '<Task %s>' % self.task_id
