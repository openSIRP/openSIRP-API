from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from Models.User import User
from Models.Ticket import Ticket
from Models.Task import Task
from Models.IoC import IoC
from Models.Tenant import Tenant
from Models.Role import Role
from Models.Permission import Permission

class UserSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = User
        exclude = ("password",)
        include_fk = True
        include_relationships = True
        load_instance = True

class TicketSchema(SQLAlchemyAutoSchema):
    class Meta:
        model= Ticket
        include_fk = True
        include_relationships = True
        load_instance = True

class TaskSchema(SQLAlchemyAutoSchema):
    class Meta:
        model= Task
        include_fk = True
        include_relationships = True
        load_instance = True

class IoCSchema(SQLAlchemyAutoSchema):
    class Meta:
        model= IoC
        include_fk = True
        include_relationships = True
        load_instance = True

class TenantSchema(SQLAlchemyAutoSchema):
    class Meta:
        model= Tenant
        include_fk = True
        include_relationships = True
        load_instance = True

class RoleSchema(SQLAlchemyAutoSchema):
    class Meta:
        model= Role
        include_fk = True
        include_relationships = True
        load_instance = True

class PermissionSchema(SQLAlchemyAutoSchema):
    class Meta:
        model= Permission
        include_fk = True
        include_relationships = True
        load_instance = True

user_schema = UserSchema()
users_schema = UserSchema(many=True)
ticket_schema = TicketSchema()
tickets_schema = TicketSchema(many=True)
task_schema = TaskSchema()
tasks_schema = TaskSchema(many=True)
ioc_schema = IoCSchema()
iocs_schema = IoCSchema(many=True)
tenant_schema = TenantSchema()
tenants_schema = TenantSchema(many=True)
role_schema = RoleSchema()
roles_schema = RoleSchema(many=True)
permission_schema = PermissionSchema()
permissions_schema = PermissionSchema(many=True)