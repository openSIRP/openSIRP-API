from app import db, ma


class Permission(db.Model):
    __tablename__ = "permissions"
    permission_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))


    def __repr__(self):
        return '<Permission %s>' % self.permission_id