from app import db, ma
from Models.Permission import Permission

roles_permissions = db.Table('roles_permissions_mapping',
    db.Column('role_id', db.Integer, db.ForeignKey('roles.role_id'), primary_key=True),
    db.Column('permission_id', db.Integer, db.ForeignKey('permissions.permission_id'), primary_key=True)
)

class Role(db.Model):
    __tablename__ = "roles"
    role_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    tenant_id = db.Column(db.Integer, db.ForeignKey("tenants.tenant_id"))
    tenant = db.relationship("Tenant", backref="roles")
    permissions = db.relationship('Permission', secondary=roles_permissions, lazy='subquery',
        backref=db.backref('roles', lazy=True))

    def __repr__(self):
        return '<Role %s>' % self.role_id