from app import db, ma

class Tenant(db.Model):
    __tablename__ = "tenants"
    tenant_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))

    def __repr__(self):
        return '<Tenant %s>' % self.name