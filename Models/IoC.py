from app import db, ma
from Models.Tenant import Tenant

class IoC(db.Model):
    __tablename__ = "iocs"
    ioc_id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)
    type = db.Column(db.String(50))
    update = db.Column(db.DateTime)
    creation = db.Column(db.DateTime)
    tenant_id = db.Column(db.Integer, db.ForeignKey("tenants.tenant_id"))
    tenant = db.relationship("Tenant", backref="iocs")

    def __repr__(self):
        return '<IoC %s>' % self.ioc_id
