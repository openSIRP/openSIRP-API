from app import db, ma
from Models.User import User
from Models.IoC import IoC
from Models.Tenant import Tenant

tickets_iocs = db.Table('tickets_iocs_mapping',
    db.Column('ticket_id', db.Integer, db.ForeignKey('tickets.ticket_id'), primary_key=True),
    db.Column('ioc_id', db.Integer, db.ForeignKey('iocs.ioc_id'), primary_key=True)
)

tickets_users = db.Table('tickets_users_mapping',
    db.Column('ticket_id', db.Integer, db.ForeignKey('tickets.ticket_id'), primary_key=True),
    db.Column('user_id', db.Integer, db.ForeignKey('users.user_id'), primary_key=True)
)


class Ticket(db.Model):
    __tablename__ = "tickets"
    ticket_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    priority = db.Column(db.String(50))
    severity = db.Column(db.String(50))
    description = db.Column(db.Text)
    update = db.Column(db.DateTime)
    creation = db.Column(db.DateTime)
    status = db.Column(db.String(50))
    iocs = db.relationship('IoC', secondary=tickets_iocs, lazy='subquery',
        backref=db.backref('tickets', lazy=True))
    users = db.relationship('User', secondary=tickets_users, lazy='subquery',
        backref=db.backref('tickets', lazy=True))
    tenant_id = db.Column(db.Integer, db.ForeignKey("tenants.tenant_id"))
    tenant = db.relationship("Tenant", backref="tickets")

    def __repr__(self):
        return '<Ticket %s>' % self.title

