from app import db, ma
from werkzeug.security import check_password_hash
from Models.Tenant import Tenant
from Models.Role import Role

tenants_users = db.Table('tenants_users_mapping',
    db.Column('tenant_id', db.Integer, db.ForeignKey('tenants.tenant_id'), primary_key=True),
    db.Column('user_id', db.Integer, db.ForeignKey('users.user_id'), primary_key=True)
)

roles_users = db.Table('roles_users_mapping',
    db.Column('role_id', db.Integer, db.ForeignKey('roles.role_id'), primary_key=True),
    db.Column('user_id', db.Integer, db.ForeignKey('users.user_id'), primary_key=True)
)

class User(db.Model):
    __tablename__ = "users"
    user_id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(50))
    lastname = db.Column(db.String(50))
    update = db.Column(db.DateTime)
    creation = db.Column(db.DateTime)
    username = db.Column(db.String(255), unique=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    tenants = db.relationship('Tenant', secondary=tenants_users, lazy='subquery',
        backref=db.backref('users', lazy=True))
    roles = db.relationship('Role', secondary=roles_users, lazy='subquery',
        backref=db.backref('users', lazy=True))

    def __repr__(self):
        return '<User %s>' % self.username

    def check_password(self, password):
        return check_password_hash(self.password, password)

