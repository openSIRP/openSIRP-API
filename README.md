# OpenSIRP-API

OpenSIRP is an OpenSource SIRP (Security Incident Response Plateform). This repository is the backend of the project.

## Get Started

### Requirements
Before install backend, you need:
- python 3.10+
- python-pip


### Install
In command terminal:

``` 
git clone https://gitea.verbaere.com/openSIRP/openSIRP-API.git 
cd openSIRP-API
```

*Optional (If you want run in python virtual env)*


``` 
python3 -m venv en 
source env/bin/activate
```

Install lib with pip
``` 
pip install -r requirements.txt 
```

### Configure
Open ```config.py``` file on set you personal settings

### Run
Launch the API
``` 
python -m flask run 
```