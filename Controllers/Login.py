import datetime
from flask import request, jsonify
from flask_restful import Resource, abort
from Models.User import User
from app import app, jwt
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required, JWTManager, current_user, create_refresh_token

# Register a callback function that takes whatever object is passed in as the
# identity when creating JWTs and converts it to a JSON serializable format.
@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.id


# Register a callback function that loads a user from your database whenever
# a protected route is accessed. This should return any python object on a
# successful lookup, or None if the lookup failed for any reason (for example
# if the user has been deleted from the database).
@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return User.query.filter_by(id=identity).one_or_none()

class Login(Resource):
    def get(self, ):
        user = User.query.filter_by(email=request.json['email']).first_or_404()

        if not user or not user.check_password(request.json['password']):
            abort(401, message='Unauthorized')
        access_token = create_access_token(identity=user)
        refresh_token = create_refresh_token(identity=user)

        return jsonify(access_token=access_token, refresh_token=refresh_token)


class Refresh(Resource):
    @jwt_required(refresh=True)
    def get(self, ):
        identity = get_jwt_identity()
        access_token = create_access_token(identity=identity)
        return jsonify(access_token=access_token)