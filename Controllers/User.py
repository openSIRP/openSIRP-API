from flask import request
from Models.User import User
from Models.Role import Role
from Models.Tenant import Tenant
from Models.Schema import user_schema, users_schema
from flask_restful import Resource, abort
from app import db
from datetime import datetime
from werkzeug.security import generate_password_hash
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required, JWTManager, current_user

class UserListResource(Resource):
    @jwt_required()
    def get(self):
        users = User.query.all()
        return users_schema.dump(users)

    @jwt_required()
    def post(self):
        tenants_array= []
        for tenant_id in request.json['tenants']:
            tenants_array.append(Tenant.query.filter_by(id=tenant_id).first())
        roles_array= []
        for role_id in request.json['roles']:
            roles_array.append(Role.query.filter_by(id=role_id).first())
        new_user = User(
            firstname=request.json['firstname'],
            lastname=request.json['lastname'],
            username=request.json['username'],
            email=request.json['email'],
            password=generate_password_hash(request.json['password']),
            tenants=tenants_array,
            roles=roles_array,
            creation=datetime.now(),
            update=datetime.now()
        )
        db.session.add(new_user)
        db.session.commit()
        return user_schema.dump(new_user)


class UserResource(Resource):
    @jwt_required()
    def get(self, user_id):
        user = User.query.get_or_404(user_id)
        return user_schema.dump(post)

    @jwt_required()
    def put(self, user_id):
        user = User.query.get_or_404(user_id)

        tenants_array= []
        for tenant_id in request.json['tenants']:
            tenants_array.append(Tenant.query.filter_by(id=tenant_id).first())
        roles_array= []
        for role_id in request.json['roles']:
            roles_array.append(Role.query.filter_by(id=role_id).first())

        user.firstname=request.json['firstname']
        user.lastname=request.json['lastname']
        user.username=request.json['username']
        user.email=request.json['email']
        user.password=generate_password_hash(request.json['password'])
        user.tenants=tenants_array
        user.roles=roles_array
        user.creation=datetime.now()
        user.update=datetime.now()

        db.session.commit()
        return user_schema.dump(post)

    @jwt_required()
    def patch(self, user_id):
        user = User.query.get_or_404(user_id)

        if 'firstname' in request.json:
            user.firstname=request.json['firstname']
        if 'lastname' in request.json:
            user.lastname=request.json['lastname']
        if 'username' in request.json:
            user.username=request.json['username']
        if 'email' in request.json:
            user.email=request.json['email']
        if 'password' in request.json:
            user.password=generate_password_hash(request.json['password'])
        if 'tenants' in request.json:
            tenants_array= []
            for tenant_id in request.json['tenants']:
                tenants_array.append(Tenant.query.filter_by(id=tenant_id).first())
            user.tenants=tenants_array
        if 'roles' in request.json:
            roles_array= []
            for role_id in request.json['roles']:
                roles_array.append(Role.query.filter_by(id=role_id).first())
            user.roles=roles_array
        
        user.update=datetime.now()

        db.session.commit()
        return user_schema.dump(post)

    @jwt_required()
    def delete(self, user_id):
        user = User.query.get_or_404(user_id)
        db.session.delete(user)
        db.session.commit()
        return '', 204