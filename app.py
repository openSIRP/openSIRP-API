#!/usr/bin/env python
import datetime
import jwt
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_restful import Api, abort
from werkzeug.security import generate_password_hash, check_password_hash
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required, JWTManager
import config

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)
jwt = JWTManager(app)

from Controllers.User import UserListResource, UserResource
from Controllers.Login import Login, Refresh

api.add_resource(UserListResource, '/v1/user')
api.add_resource(UserResource, '/v1/user/<int:post_id>')
api.add_resource(Login, '/v1/login')
api.add_resource(Refresh, '/v1/refresh')
db.create_all()


if __name__ == '__main__':
    app.run()