from datetime import timedelta

DEBUG = True
JWT_SECRET_KEY = "SECRET"
SQLALCHEMY_DATABASE_URI='sqlite:///test.db'
JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=24)
JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=30)